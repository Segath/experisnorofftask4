﻿using System;
using System.Collections.Generic;

namespace Task4_DrawRectV2
{
    class Program
    {
        /* 
         * This program uses 2d matrices to create the rectangles.
         * It will create a new 2d matrix for each possible size of rectangles
         * and merges them into a larger one.
        */
        static void Main(string[] args)
        {
            #region variables and user input
            int width = UserInput("Type the width: "); // #Columns, pos 1
            int height = UserInput("Type the height: "); // #Rows, pos 0
            
            // 4 is the number the height and width should scale 
            // with in order to create an inner rectangle
            int reductionNum = 4;

            string[,] rect = new string[0, 0];
            #endregion

            #region construct the rectangle matrix
            Console.Write("Do you want it to draw multiple rectangles while following the rules? (y/n): ");

            #region multiple rectangles
            if (Console.ReadLine() == "y")
            {
                // The least amount of times width and height can be reduced by 4
                int amountOfReductions = 0;
                while (true)
                {
                    if (width - reductionNum > 0 && height - reductionNum > 0)
                    {
                        amountOfReductions++;
                    }
                    else
                    {
                        break;
                    }
                    reductionNum += 4;
                }

                // Create the rectangle matrix
                for (int i = amountOfReductions; i >= 0; i--)
                {
                    if (i == amountOfReductions)
                    {
                        rect = CreateRect(width - (i * 4), height - (i * 4));
                    }
                    else
                    {
                        // Combines a larger rectangle matrix with the smaller one
                        rect = CombineRect(CreateRect(width - (i * 4), height - (i * 4)), rect);
                    }
                }
            }
            #endregion

            #region outer and inner rectangle
            else
            {
                rect = CombineRect(CreateRect(width, height), CreateRect(width - reductionNum, height - reductionNum));
            }
            #endregion
            #endregion

            printRect(rect);
        }


        // Creates and returns a rectangle matrix by the specified size 
        public static string[,] CreateRect(int width, int height)
        {
            if (width < 0 || height < 0)
            {
                width = 0;
                height = 0;
            }

            string[,] matrixRect = new string[height, width];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    // Creates the full lines at the top and bottom
                    if (i == 0 || i == height - 1)
                    {
                        matrixRect[i, j] = "#";
                    }
                    // Creates the lines at the sides
                    else if (j == 0 || j == width - 1)
                    {
                        matrixRect[i, j] = "#";
                    }
                    else
                    {
                        matrixRect[i, j] = " ";
                    }
                }
            }
            return matrixRect;
        }


        // Places the smaller rectangle into the larger one, and returns the combined result
        public static string[,] CombineRect(string[,] largeRect, string[,] smallRect)
        {
            for (int i = 0; i < smallRect.GetLength(0); i++)
            {
                for (int j = 0; j < smallRect.GetLength(1); j++)
                {
                    largeRect[i+2, j+2] = smallRect[i, j];
                }
            }

            return largeRect;
        }


        // Iterates over the matrix, and prints it as a 2d rectangle
        public static void printRect(string[,] rect)
        {
            for (int i = 0; i < rect.GetLength(0); i++)
            {
                for (int j = 0; j < rect.GetLength(1); j++)
                {
                    Console.Write(rect[i,j]);
                }
                Console.WriteLine();
            }
        }


        // Exception handling for user input
        public static int UserInput(string msg)
        {
            int value = -1;
            do
            {
                Console.Write(msg);
                try
                {
                    value = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Type a number");
                }
            } while (value == -1);

            return value;
        }
    }
}
